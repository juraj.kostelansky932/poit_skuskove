#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <ArduinoJson.h>

#define DHTPIN 2
#define DHTTYPE DHT11

DHT_Unified dht(DHTPIN, DHTTYPE);

bool monitoring = false;

void setup() {
  Serial.begin(9600);
  dht.begin();
  Serial.println(F("DHTxx Unified Sensor Example"));

  sensor_t sensor;
  // dht.temperature().getSensor(&sensor);
  // Serial.println(F("------------------------------------"));
  // Serial.println(F("Temperature Sensor"));
  // Serial.print(F("Sensor Type: ")); Serial.println(sensor.name);
  // Serial.print(F("Driver Ver:  ")); Serial.println(sensor.version);
  // Serial.print(F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  // Serial.print(F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("°C"));
  // Serial.print(F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("°C"));
  // Serial.print(F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("°C"));
  // Serial.println(F("------------------------------------"));

  // dht.humidity().getSensor(&sensor);
  // Serial.println(F("Humidity Sensor"));
  // Serial.print(F("Sensor Type: ")); Serial.println(sensor.name);
  // Serial.print(F("Driver Ver:  ")); Serial.println(sensor.version);
  // Serial.print(F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  // Serial.print(F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("%"));
  // Serial.print(F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("%"));
  // Serial.print(F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("%"));
  // Serial.println(F("------------------------------------"));
}

void loop() {
  if (Serial.available() > 0) {
    String command = Serial.readStringUntil('\n');
    if (command == "START") {
      monitoring = true;
      Serial.println("Monitoring started");
    } else if (command == "STOP") {
      monitoring = false;
      Serial.println("Monitoring stopped");
    } else {
      Serial.println("XNA");
    }
  }

  if (monitoring) {
    sensors_event_t event;
    float temperature = NAN;
    float humidity = NAN;

    dht.temperature().getEvent(&event);
    if (!isnan(event.temperature)) {
      temperature = event.temperature;
    }

    dht.humidity().getEvent(&event);
    if (!isnan(event.relative_humidity)) {
      humidity = event.relative_humidity;
    }

    StaticJsonDocument<200> jsonDoc;
    jsonDoc["temperature"] = temperature;
    jsonDoc["humidity"] = humidity;

    char jsonBuffer[512];
    serializeJson(jsonDoc, jsonBuffer);

    Serial.println(jsonBuffer);
    delay(500);
  }
}
