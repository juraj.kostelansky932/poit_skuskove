from flask import Flask, render_template, jsonify, request
from flask_socketio import SocketIO
import serial
import threading
import time
import json
import os
import mysql.connector
from datetime import datetime

app = Flask(__name__)
socketio = SocketIO(app)

arduino_port = '/dev/cu.usbmodem11301'
arduino_baudrate = 9600
arduino_timeout = 1
arduino = None
sensor_data_buffer = []

monitoring = False

json_file_path = 'measurement_data.json'
if not os.path.exists(json_file_path):
    with open(json_file_path, 'w') as f:
        json.dump([], f)

def save_data_to_file(file_path, data):
    with open(file_path, 'r+') as file:
        try:
            existing_data = json.load(file)
        except json.JSONDecodeError:
            existing_data = []
        existing_data.extend(data)
        file.seek(0)
        json.dump(existing_data, file, indent=4)
        file.truncate()
    print("Data saved to JSON")

def read_arduino_data():
    global arduino, monitoring, sensor_data_buffer
    while arduino:
        if monitoring and arduino.in_waiting > 0:
            line = arduino.readline().decode('utf-8').rstrip()
            if line:
                print(f"Raw data from Arduino: {line}")
                try:
                    data = json.loads(line)
                    temperature = data.get("temperature")
                    humidity = data.get("humidity")
                    if temperature is not None and humidity is not None:
                        timestamp = datetime.now().isoformat()
                        data_with_time = {
                            "temperature": temperature,
                            "humidity": humidity,
                            "time": timestamp
                        }
                        socketio.emit('sensor_data', data_with_time)
                        sensor_data_buffer.append(data_with_time)
                        if len(sensor_data_buffer) >= 10:
                            save_data_to_file(json_file_path, sensor_data_buffer)
                            save_data_to_database(sensor_data_buffer)
                            sensor_data_buffer = []
                        print(f"Emitted data: {data_with_time}")
                except json.JSONDecodeError as e:
                    print(f"Error decoding JSON data: {e}")
                except ValueError as e:
                    print(f"Error reading data: {e}")
        socketio.sleep(0.5)

def connect_arduino(port, baudrate, timeout):
    global arduino
    while True:
        try:
            arduino = serial.Serial(port, baudrate, timeout=timeout)
            print(f"Connected to Arduino on port {port}")
            socketio.emit('arduino_status', {'message': f'Connected to Arduino on port {port}'})
            return arduino
        except serial.SerialException as e:
            if 'Resource busy' in str(e):
                print(f"Port {port} is busy. Retrying in 2 seconds...")
                time.sleep(2)
            else:
                print(f"Failed to connect to Arduino: {e}")
                time.sleep(2)

def disconnect_arduino():
    global arduino
    if arduino:
        arduino.close()
        arduino = None
        print("Disconnected from Arduino")
        socketio.emit('arduino_status', {'message': 'Disconnected from Arduino'})

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/system_open', methods=['POST'])
def initialize_system():
    try:
        connect_arduino(arduino_port, arduino_baudrate, arduino_timeout)
        threading.Thread(target=read_arduino_data).start()
        return jsonify({"status": "success", "message": "System initialized"}), 200
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500

@app.route('/monitor_start', methods=['POST'])
def start_monitoring():
    global monitoring
    monitoring = True
    if arduino:
        arduino.write(b'START\n')
    print("Monitoring started") 
    socketio.emit('arduino_status', {'message': 'Monitoring started'})
    return jsonify({"status": "success", "message": "Monitoring started"}), 200

@app.route('/monitor_stop', methods=['POST'])
def stop_monitoring():
    global monitoring
    monitoring = False
    if arduino:
        arduino.write(b'STOP\n')
    print("Monitoring stopped") 
    socketio.emit('arduino_status', {'message': 'Monitoring stopped'})
    return jsonify({"status": "success", "message": "Monitoring stopped"}), 200

@app.route('/system_disconnect', methods=['POST'])
def disconnect_system():
    try:
        disconnect_arduino()
        return jsonify({"status": "success", "message": "Disconnected from Arduino"}), 200
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500

@app.route('/fetch_json_data', methods=['GET'])
def get_data_from_json():
    try:
        with open(json_file_path, 'r') as f:
            data = json.load(f)
        return jsonify(data), 200
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500

@app.route('/fetch_db_data', methods=['GET'])
def get_data_from_db():
    try:
        db = mysql.connector.connect(
            host="localhost",
            user="root",
            password="slovakia",
            database="sensor_data"
        )
        cursor = db.cursor(dictionary=True)
        cursor.execute("SELECT * FROM weather_data")
        data = cursor.fetchall()
        cursor.close()
        db.close()
        return jsonify(data), 200
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500

def save_data_to_database(data):
    db = mysql.connector.connect(
        host="localhost",
        user="root",
        password="slovakia",
        database="sensor_data"
    )
    cursor = db.cursor()

    for entry in data:
        sql = "INSERT INTO weather_data (humidity, temperature, time) VALUES (%s, %s, %s)"
        val = (entry["humidity"], entry["temperature"], entry["time"])
        cursor.execute(sql, val)

    db.commit()
    cursor.close()
    db.close()

if __name__ == '__main__':
    socketio.run(app, debug=True)
